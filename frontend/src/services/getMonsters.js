import axios from 'axios';

export async function getMonsters() {
  const cached = localStorage.getItem('dndfight/monsters');
  if (cached) {
    return JSON.parse(cached);
  }

  const monsters = await axios.get(
    'https://dndfight-functions.azurewebsites.net/api/GetMonsters?code=LASUHixFJvTiCrY7Bjy2Y9Wc9i5qwZuw8BgbnFvnEKu2cpeQx09L/g=='
  );
  localStorage.setItem('dndfight/monsters', JSON.stringify(monsters.data));
  return monsters.data;
}
