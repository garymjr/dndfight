using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DndFight.GetMonsters
{
    public static class GetMonsters
    {

        private static string EndpointUri = Environment.GetEnvironmentVariable("ENDPOINT_URI");
        private static string ApiKey = Environment.GetEnvironmentVariable("API_KEY");

        [FunctionName("GetMonsters")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            var client = new DocumentClient(new Uri(EndpointUri), ApiKey);
            var collection = await client.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri("DndFight", "Monster"));
            var monsterQuery = client.CreateDocumentQuery(UriFactory.CreateDocumentCollectionUri("DndFight", "Monster"));
            var monsters = monsterQuery.ToArray();

            return new OkObjectResult(monsters);
        }
    }
}
