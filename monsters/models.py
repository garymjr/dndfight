from django.db import models


class Alignment(models.Model):
    name = models.CharField(max_length=100, unique=True)
    short_name = models.CharField(max_length=2)

    def __str__(self):
        if self.short_name != "":
            return f"{self.name} ({self.short_name})"
        else:
            return self.name


class Tag(models.Model):
    name = models.CharField(max_length=100, unique=True)


class Source(models.Model):
    name = models.CharField(max_length=100)
    page = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.name}: {self.page}"

    class Meta:
        unique_together = (("name", "page"),)


class MonsterType(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class MonsterSize(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Environment(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class ChallengeRating(models.Model):
    name = models.CharField(max_length=100, unique=True)
    value = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.name} - {self.value}"


class Monster(models.Model):
    # Basic monster information
    name = models.CharField(max_length=100, unique=True)
    alignment = models.ForeignKey(Alignment, on_delete=models.PROTECT)
    challenge_rating = models.ForeignKey(ChallengeRating, on_delete=models.PROTECT)
    health_points = models.IntegerField(default=10, blank=True)
    armor_class = models.IntegerField(default=10, blank=True)
    initiative_mod = models.IntegerField(default=0, blank=True)
    monster_type = models.ForeignKey(MonsterType, on_delete=models.PROTECT)
    monster_size = models.ForeignKey(MonsterSize, on_delete=models.PROTECT)
    # Optional information
    source = models.ManyToManyField(Source, default=None, blank=True)
    tags = models.ManyToManyField(Tag, default=None, blank=True)
    environment = models.ManyToManyField(Environment, default=None, blank=True)
    # Special options
    legendary = models.BooleanField(default=False, blank=True)
    unique = models.BooleanField(default=False, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return (
            f"{self.name} {self.challenge_rating} {self.alignment.short_name.upper()}"
        )
