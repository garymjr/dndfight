from rest_framework import viewsets

from .models import (
    Monster,
    Alignment,
    Tag,
    Source,
    MonsterType,
    MonsterSize,
    Environment,
    ChallengeRating,
)
from .serializers import (
    MonsterSerializer,
    AlignmentSerializer,
    TagSerializer,
    SourceSerializer,
    MonsterTypeSerializer,
    MonsterSizeSerializer,
    EnvironmentSerializer,
    ChallengeRatingSerializer,
)


class MonsterViewSet(viewsets.ModelViewSet):
    queryset = Monster.objects.all()
    serializer_class = MonsterSerializer


class AlignmentViewSet(viewsets.ModelViewSet):
    queryset = Alignment.objects.all()
    serializer_class = AlignmentSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class SourceViewSet(viewsets.ModelViewSet):
    queryset = Source.objects.all()
    serializer_class = SourceSerializer


class MonsterTypeViewSet(viewsets.ModelViewSet):
    queryset = MonsterType.objects.all()
    serializer_class = MonsterTypeSerializer


class MonsterSizeViewSet(viewsets.ModelViewSet):
    queryset = MonsterSize.objects.all()
    serializer_class = MonsterSizeSerializer


class EnvironmentViewSet(viewsets.ModelViewSet):
    queryset = Environment.objects.all()
    serializer_class = EnvironmentSerializer


class ChallengeRatingViewSet(viewsets.ModelViewSet):
    queryset = ChallengeRating.objects.all()
    serializer_class = ChallengeRatingSerializer
