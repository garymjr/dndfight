from django.shortcuts import render

from monsters.models import Monster

def get_monsters(request):
    monsters = Monster.objects.all()
    return render(request, 'monsters/monsters.html', {'monsters':monsters})