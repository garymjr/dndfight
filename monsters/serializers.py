from rest_framework import serializers

from .models import (
    Monster,
    Alignment,
    Tag,
    Source,
    MonsterType,
    MonsterSize,
    Environment,
    ChallengeRating,
)


class MonsterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Monster
        fields = (
            "id",
            "name",
            "alignment",
            "challenge_rating",
            "health_points",
            "armor_class",
            "initiative_mod",
            "monster_type",
            "source",
            "tags",
            "environment",
            "legendary",
            "unique",
        )


class AlignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alignment
        fields = ("id", "name", "short_name")


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ("id", "name")


class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ("id", "name", "page")


class MonsterTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MonsterType
        fields = ("id", "name")


class MonsterSizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MonsterSize
        fields = ("id", "name")


class EnvironmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Environment
        fields = ("id", "name")


class ChallengeRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChallengeRating
        fields = ("id", "name", "value")
