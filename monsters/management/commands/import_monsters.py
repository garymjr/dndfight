from django.core.management.base import BaseCommand

import simplejson as json
from monsters.models import (
    Monster,
    Alignment,
    MonsterType,
    MonsterSize,
    Source,
    Environment,
    Tag,
    ChallengeRating,
)


class Command(BaseCommand):
    help = """
    Imports monsters from json file
    """

    monsters = None

    def get_short_name(self, alignment):
        alignments = {
            "lawful good": "lg",
            "lawful neutral": "ln",
            "lawful evil": "le",
            "neutral good": "ng",
            "neutral": "n",
            "neutral evil": "ne",
            "chaotic good": "cg",
            "chaotic neutral": "cn",
            "chaotic evil": "ce",
            "unaligned": "u",
            "any": "a",
            "non-good": "",
            "non-lawful": "",
            "any chaotic": "ac",
            "any evil": "ae",
            "neutral good (50%) or neutral evil (50%)": "",
            "chaotic good (75%) or neutral evil (25%)": "",
            "any alignment": "a",
        }
        return alignments[alignment]

    def get_sources(self, sources):
        if sources is None:
            return []
        sources_list = []
        sources = sources.split(", ")
        for s in sources:
            s = s.split(": ")
            sources_list.append({"name": s[0], "page": s[1]})
        return sources_list

    def get_environment(self, environment):
        if environment is None:
            return []
        environment_list = []
        environment = environment.split(", ")
        for e in environment:
            environment_list.append(e)
        return environment_list

    def get_tags(self, tags):
        if tags is None:
            return []
        tags_list = []
        tags = tags.split(", ")
        for t in tags:
            tags_list.append(t)
        return tags_list

    def get_challenge_rating(self, cr):
        rating_value = {
            "0": 10,
            "1/8": 25,
            "1/4": 50,
            "1/2": 100,
            "1": 200,
            "2": 450,
            "3": 700,
            "4": 1100,
            "5": 1800,
            "6": 2300,
            "7": 2900,
            "8": 3900,
            "9": 5000,
            "10": 5900,
            "11": 7200,
            "12": 8400,
            "13": 10000,
            "14": 11500,
            "15": 13000,
            "16": 15000,
            "17": 18000,
            "18": 20000,
            "19": 22000,
            "20": 25000,
            "21": 33000,
            "22": 41000,
            "23": 50000,
            "24": 62000,
            "25": 75000,
            "26": 90000,
            "27": 105000,
            "28": 120000,
            "29": 135000,
            "30": 155000,
        }
        return (cr, rating_value[cr])

    def get_health_points(self, hp):
        return int(hp)

    def get_armor_class(self, ac):
        if " " in ac:
            ac = ac.split(" ")
            return int(ac[0].replace(",", ""))
        else:
            return int(ac)

    def import_monster(self, monster):
        alignment, _ = Alignment.objects.update_or_create(
            name=monster["alignment"],
            defaults={
                "name": monster["alignment"],
                "short_name": self.get_short_name(monster["alignment"]),
            },
        )

        monster_type, _ = MonsterType.objects.update_or_create(
            name=monster["type"], defaults={"name": monster["type"]}
        )
        monster_size, _ = MonsterSize.objects.update_or_create(
            name=monster["size"], defaults={"name": monster["size"]}
        )

        cr, cr_value = self.get_challenge_rating(monster["cr"])
        challenge_rating, _ = ChallengeRating.objects.update_or_create(
            name=cr, defaults={"name": cr, "value": cr_value}
        )

        health_points = self.get_health_points(monster["hp"])
        armor_class = self.get_armor_class(monster["ac"])

        if monster.get("legendary", False):
            legendary = True
        else:
            legendary = False

        if monster.get("unique", False):
            unique = True
        else:
            unique = False

        monster_obj, _ = Monster.objects.update_or_create(
            name=monster["name"],
            defaults={
                "name": monster["name"],
                "alignment": alignment,
                "challenge_rating": challenge_rating,
                "health_points": health_points,
                "armor_class": armor_class,
                "initiative_mod": monster["init"],
                "monster_type": monster_type,
                "monster_size": monster_size,
                "legendary": legendary,
                "unique": unique,
            },
        )

        for s in self.get_sources(monster.get("sources", None)):
            source, _ = Source.objects.update_or_create(
                name=s["name"],
                page=s["page"],
                defaults={"name": s["name"], "page": s["page"]},
            )
            monster_obj.source.add(source)

        for e in self.get_environment(monster.get("environment", None)):
            environment, _ = Environment.objects.update_or_create(
                name=e, defaults={"name": e}
            )
            monster_obj.environment.add(environment)

        for t in self.get_tags(monster.get("tags", None)):
            tag, _ = Tag.objects.update_or_create(name=t, defaults={"name": t})
            monster_obj.tags.add(tag)

        print(monster_obj)

    def handle(self, *args, **options):
        with open("monsters.json") as f:
            monster_file = f.read()

        self.monsters = json.loads(monster_file)
        for monster in self.monsters:
            self.import_monster(monster)
