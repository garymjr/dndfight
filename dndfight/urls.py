from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

import monsters.views
from monsters.viewsets import (
    MonsterViewSet,
    AlignmentViewSet,
    TagViewSet,
    SourceViewSet,
    MonsterTypeViewSet,
    MonsterSizeViewSet,
    EnvironmentViewSet,
    ChallengeRatingViewSet,
)


router = routers.DefaultRouter()
router.register(r"monsters", MonsterViewSet, base_name="monsters")
router.register(r"alignment", AlignmentViewSet)
router.register(r"tag", TagViewSet)
router.register(r"source", SourceViewSet)
router.register(r"monster_type", MonsterTypeViewSet)
router.register(r"monster_size", MonsterSizeViewSet)
router.register(r"environment", EnvironmentViewSet)
router.register(r"challenge_rating", ChallengeRatingViewSet)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api-auth", include("rest_framework.urls")),
    path("", include(router.urls)),
    path("get_monsters", monsters.views.get_monsters, name="get_monsters")
]
